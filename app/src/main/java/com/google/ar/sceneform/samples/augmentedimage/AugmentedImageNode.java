/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.sceneform.samples.augmentedimage;

import android.app.Activity;
import android.net.Uri;
import android.widget.TextView;

import com.callibrity.ar.entities.Product;
import com.callibrity.config.ArConfiguration;
import com.callibrity.tasks.WebTask;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.io.IOException;

/**
 * Node for rendering an augmented image. The image is framed by placing the virtual picture frame
 * at the corners of the augmented image trackable.
 */
@SuppressWarnings({"AndroidApiChecker"})
public class AugmentedImageNode extends AnchorNode {

    private static final String TAG = "AugmentedImageNode";
    private final Activity activity;

    public AugmentedImageNode(Activity activity) {
        this.activity = activity;
    }

    // The augmented image represented by this node.
    private AugmentedImage image;

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image. The corners are then positioned based on the
     * extents of the image. There is no need to worry about world coordinates since everything is
     * relative to the center of the image, which is the parent node of the corners.
     */
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image, String searchQuery) {
        ArConfiguration.confirmLocation();
        this.image = image;

        WebTask webTask = new WebTask(this::onComplete);
        if (searchQuery == null || searchQuery.isEmpty()){
            String url = String.valueOf(image.getIndex());
            if (ArConfiguration.getPreferences(activity).length() > 0) {
                url += "?";
                for (String preference : ArConfiguration.getPreferences(activity).split("\\|")) {
                    url += "attributes=" + preference + "&";
                }
            }
            webTask.execute(url);
        } else {
            webTask.execute(String.valueOf(image.getIndex()) + "?name=" + Uri.encode(searchQuery));
        }
    }

    private void onComplete(String result) {
        if (!image.getTrackingState().equals(TrackingState.TRACKING)) {
            return;
        }
        try {
            Product[] products = new ObjectMapper().readValue(result, Product[].class);
            for (Product product : products) {

                // Set the anchor based on the center of the image.q
                setAnchor(image.createAnchor(image.getCenterPose()));

                // Make the 4 corner nodes.
                Vector3 localPosition = new Vector3();

                // Upper left corner.
                localPosition.set(
                        product.getCoor().x,
                        product.getCoor().y,
                        product.getCoor().z);
                Node currentNode = new Node();
                currentNode.setParent(this);
                currentNode.setLocalPosition(localPosition);
                currentNode.setRenderable(ArConfiguration.getHalo());
                currentNode.setLocalScale(new Vector3(.01f, .01f, .01f));
                currentNode.setLocalRotation(Quaternion.axisAngle(Vector3.right(), 90));
                Node renderableNode = new Node();
                renderableNode.setParent(currentNode);
                renderableNode.setLocalPosition(new Vector3(0, 0, -2));
                renderableNode.setLocalScale(new Vector3(50, 50, 50));
                renderableNode.setLocalRotation(Quaternion.axisAngle(Vector3.left(), 90));
                ViewRenderable.builder()
                        .setView(activity, R.layout.product_widget)
                        .build()
                        .thenAccept(viewRenderable -> {
                            ((TextView) viewRenderable.getView()).setText(product.getName());
                            renderableNode.setRenderable(viewRenderable);
                        });
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse server result", e);
        }
    }

    public AugmentedImage getImage() {
        return image;
    }
}
