package com.google.ar.sceneform.samples.augmentedimage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.callibrity.config.ArConfiguration;
import com.callibrity.tasks.WebTask;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class PreferencesFragment extends Fragment {
    private LinearLayout myList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_preferences, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        WebTask webTask = new WebTask(this::onComplete);
        webTask.execute("attributes");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void onComplete(String result) {
        myList = getView().findViewById(R.id.checkbox_list);
        String[] attributes = parseAttributes(result);
        for (String attribute : attributes) {
            CheckBox cb = createCheckBox(attribute);
            myList.addView(cb);
        }
    }

    private String[] parseAttributes(String result) {
        String[] attributes;
        try {
            attributes = new ObjectMapper().readValue(result, String[].class);
        } catch (IOException e) {
            Log.e("PreferencesFragment", "Unable to parse server response! ", e);
            attributes = new String[]{};
        }
        return attributes;
    }

    @NonNull
    private CheckBox createCheckBox(String attribute) {
        attribute = attribute.replace("\"", "");
        CheckBox cb = new CheckBox(getActivity());
        cb.setText(attribute);
        if(getPreferences().contains(attribute + "|")){
            cb.setChecked(true);
        }
        cb.setOnCheckedChangeListener(getOnCheckedChangeListener());
        return cb;
    }

    @NonNull
    private CompoundButton.OnCheckedChangeListener getOnCheckedChangeListener() {
        return (buttonView, isChecked) -> {
            SharedPreferences sharedPref = getActivity().getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            String preferences = getPreferences();
            if (isChecked && !preferences.contains(buttonView.getText())) {
                String newPreferences = preferences + buttonView.getText().toString() + "|";
                editor.putString(getString(R.string.my_preferences), newPreferences);
            } else if (preferences.contains(buttonView.getText())) {
                String newPreferences = preferences.replace(buttonView.getText().toString() + "|", "");
                editor.putString(getString(R.string.my_preferences), newPreferences);
            }
            editor.apply();
        };
    }

    private String getPreferences() {
        return ArConfiguration.getPreferences(getActivity());
    }
}
