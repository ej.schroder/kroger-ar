package com.google.ar.sceneform.samples.augmentedimage;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements SearchFragment.OnFragmentSearchSubmit {
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = findViewById(R.id.main_drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(getNavigationListener());
        setupBackgroundFragment();
    }

    private void setupBackgroundFragment() {
        try {
            Fragment backgroundFragment = new BackgroundFragment();
            setFragmentInContentFrame(backgroundFragment);
        } catch (Exception e){
            setFragmentInContentFrame(new Fragment());
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onSearch(String searchText) {
        Fragment fragment = AugmentedImageOverlayFragment.newInstance(searchText);
        setFragmentInContentFrame(fragment);
    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getNavigationListener() {
        return menuItem -> {
            Fragment fragment = null;
            Class fragmentClass;
            switch(menuItem.getItemId()) {
                case R.id.nav_camera:
                    fragmentClass = AugmentedImageOverlayFragment.class;
                    break;
                case R.id.nav_preferences:
                    fragmentClass = PreferencesFragment.class;
                    break;
                case R.id.nav_search:
                    fragmentClass = SearchFragment.class;
                    break;
                default:
                    fragmentClass = BackgroundFragment.class;
            }

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            setFragmentInContentFrame(fragment);

            menuItem.setChecked(true);
            mDrawerLayout.closeDrawers();

            return true;
        };
    }

    private void setFragmentInContentFrame(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }
}
