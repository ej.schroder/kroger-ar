/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.sceneform.samples.augmentedimage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.callibrity.config.ArConfiguration;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Frame;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.samples.common.helpers.SnackbarHelper;
import com.google.ar.sceneform.ux.ArFragment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This application demonstrates using augmented images to place anchor nodes. app to include image
 * tracking functionality.
 */
public class AugmentedImageOverlayFragment extends Fragment {

    private static final String SEARCH_TEXT_PARAM = "searchText";
    private String searchParam;
    private ArFragment arFragment;
    private ImageView fitToScanView;

    // Augmented image and its associated center pose anchor, keyed by the augmented image in
    // the database.
    private final Map<AugmentedImage, AugmentedImageNode> augmentedImageMap = new HashMap<>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param searchText Search text.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AugmentedImageOverlayFragment newInstance(String searchText) {
        AugmentedImageOverlayFragment fragment = new AugmentedImageOverlayFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_TEXT_PARAM, searchText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_aug_image, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchParam = getArguments().getString(SEARCH_TEXT_PARAM);
            Log.i("AugmentedImageOverlayFragment", "Got a search string: " + searchParam);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        arFragment = (ArFragment) getChildFragmentManager().findFragmentById(R.id.ux_fragment);
        fitToScanView = getView().findViewById(R.id.image_view_fit_to_scan);

        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);
        ArConfiguration.Initialize(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (augmentedImageMap.isEmpty()) {
            fitToScanView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Registered with the Sceneform Scene object, this method is called at the start of each frame.
     *
     * @param frameTime - time since last frame.
     */
    private boolean hasDisplayedMessage = false;

    private void onUpdateFrame(FrameTime frameTime) {
        if (!ArConfiguration.isLocationConfirmed() && !SnackbarHelper.getInstance().isShowing()) {
            SnackbarHelper.getInstance().showMessage(getActivity(), "Please scan a tracker image to confirm your location.");
        }
        Frame frame = arFragment.getArSceneView().getArFrame();

        // If there is no frame or ARCore is not tracking yet, just return.
        if (frame == null) {
            return;
        }

        Collection<AugmentedImage> updatedAugmentedImages =
                frame.getUpdatedTrackables(AugmentedImage.class);
        for (AugmentedImage augmentedImage : updatedAugmentedImages) {
            switch (augmentedImage.getTrackingState()) {
                case PAUSED:
                    fitToScanView.setVisibility(View.VISIBLE);
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    if (!hasDisplayedMessage) {
                        String text = "Attempting to determine photo location...";
                        SnackbarHelper.getInstance().showMessage(getActivity(), text);
                        hasDisplayedMessage = true;
                        break;
                    }
                case TRACKING:
                    hasDisplayedMessage = false;
                    fitToScanView.setVisibility(View.GONE);
                    if (SnackbarHelper.getInstance().isShowing()) {
                        SnackbarHelper.getInstance().hide(getActivity());
                    }
                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        AugmentedImageNode node = new AugmentedImageNode(getActivity());
                        node.setImage(augmentedImage, searchParam);
                        augmentedImageMap.put(augmentedImage, node);
                        arFragment.getArSceneView().getScene().addChild(node);
                    }
                    break;

                case STOPPED:
                    hasDisplayedMessage = false;
                    fitToScanView.setVisibility(View.GONE);
                    augmentedImageMap.remove(augmentedImage);
                    break;
            }
        }
    }
}
