package com.callibrity.tasks;

import android.os.AsyncTask;

import com.callibrity.config.ArConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Consumer;

public class WebTask extends AsyncTask<String, Void, String> {
    private Consumer<String> callback;

    public WebTask(Consumer<String> callback) {
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... url) {
        if (url == null || url.length != 1) {
            throw new IllegalArgumentException("Bad number of params");
        }
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(ArConfiguration.getUrl() + "/products/" + url[0]).openConnection();
            urlConnection.connect();
            if (urlConnection.getResponseCode() != 200) {
                throw new RuntimeException("Unable to connect to server.");
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = urlConnection.getInputStream().read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, length);
                }
                return byteArrayOutputStream.toString();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        callback.accept(result);
    }
}
