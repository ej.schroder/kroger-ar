package com.callibrity.config;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.samples.augmentedimage.R;

import java.util.concurrent.CompletableFuture;

public class ArConfiguration {
    private static final String ServerUrl = "http://192.168.13.156";
    private static final String port = "3000";

    public static String getUrl() {
        return ServerUrl + ":" + port;
    }

    //selection halo
    private static CompletableFuture<ModelRenderable> halo;

    public static void Initialize(Context context) {
        if (halo == null) {
            halo = ModelRenderable.builder()
                    .setSource(context, Uri.parse("models/Ring.sfb"))
                    .build();
        }
    }

    public static ModelRenderable getHalo() {
        return halo.getNow(null);
    }

    private static boolean isLocationConfirmed = false;

    public static void confirmLocation() {
        isLocationConfirmed = true;
    }

    public static boolean isLocationConfirmed() {
        return isLocationConfirmed;
    }

    public static String getPreferences(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(activity.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getString(activity.getString(R.string.my_preferences), "");
    }
}
